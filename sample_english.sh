#!/bin/bash
#/!\ Attention: since create_samples uses random to generate the smaller sets, changing the random seed  will result on a different set, 
# and this may affect a bit the reproduction of the exact numbers reported on the paper

declare -a out=("train" "dev")
for i in "${out[@]}"
do
   echo "corpora/english-french-parallel-corpus/33k/$i/en/"
done > corpora/english33k_paths


python scripts/create_sample.py --output english5k --files-list corpora/english33k_paths --size 5130 --sets

scripts/create_folders.sh 33k 5k english-french-parallel-corpus

#for i in "${out[@]}"
#do
#   echo "corpora/english-french-parallel-corpus/5k/$i/en/"
#done > corpora/english5k_paths

#python scripts/create_sample.py --output english330 --files-list corpora/english5k_paths --size 330 --sets

#scripts/create_folders.sh 5k 330 english-french-parallel-corpus
rm corpora/english33k_paths #corpora/english5k_paths




# Empirical Evaluation of Sequence-to-Sequence Models for Word Discovery in Low-resourse Settings
This is the repository of our [Interspeech 2019 paper]().


## Corpora
Use the script **download_corpora.sh** to download all the data.
The corpora used for our experiments:

* [Mboshi-French Parallel Corpus](https://gitlab.com/mzboito/mboshi-french-parallel-corpus)
* [English-French Parallel Corpus](https://gitlab.com/mzboito/english-french-parallel-corpus)
* [ZRC reference (all corpora)](https://github.com/mzboito/ZRC_corpora)

### Down-sampling: 
For reproducing the down-sampling for the EN-FR data set, as in our paper, it suffices to execute:
```
./corpora/download_corpora.sh
./sample_english.sh
```

## Neural Architectures

* Recurrent Architecture: [LIG-CRIStAL NMT seq2seq system](https://github.com/eske/seq2seq)
* Convolutional Architecture: [Pervasive Attention 2D](https://github.com/elbayadm/attn2d)
* Transformer Architecture: [fairseq implementation](https://github.com/pytorch/fairseq)

### Parameters:
the setup/ folder gather the configuration files for the different models.

## Examples:

We also include a series of examples (sorted per Sentence ANE) inside the examples/ folder. 

<img src="entropy_example.png" width="400" height="400">


## Post-processing and Evaluation

* Soft-alignment Retrieval (SOON, contact me for more information if needed)
* [Word Segmentation](https://github.com/mzboito/word_discovery)
* [Evaluation scripts (ZRC track 2)](https://github.com/bootphon/zerospeech2017)

## Citing

@inproceedings{Boito2019,
  author={Marcely Zanon Boito and Aline Villavicencio and Laurent Besacier},
  title={{Empirical Evaluation of Sequence-to-Sequence Models for Word Discovery in Low-Resource Settings}},
  year=2019,
  booktitle={Proc. Interspeech 2019},
  pages={2688--2692},
  doi={10.21437/Interspeech.2019-2029},
  url={http://dx.doi.org/10.21437/Interspeech.2019-2029}
}


echo "bigram model"
nohup ~/tools/dpseg/segment  --ut -i20000 -a2169,42 -A76918,24 -b0,12 -U0,63 -Mboshi ~/mboshi_5k/true_phones/train+dev.unseg.phn.encoded -v3 > ~/mboshi_5k/true_phones/true_phones.dpseg.bigram &

echo "unigram model"
nohup ~/tools/dpseg/segment --mm-i20000 -a2169,42 -A76918,24 -b0,12 -U0,63 -Mboshi ~/mboshi_5k/true_phones/train+dev.unseg.phn.encoded -v3 > ~/mboshi_5k/true_phones/true_phones.dpseg.unigram &


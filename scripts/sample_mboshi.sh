#!/bin/bash
#/!\ Attention: since create_samples uses random to generate the smaller sets, changing the random seed  will result on a different set, 
# and this may affect a bit the reproduction of the exact numbers reported on the paper

declare -a out=("train" "dev")
for i in "${out[@]}"
do
   echo "corpora/mboshi-french-parallel-corpus/5k/$i/mb/"
done > corpora/mboshi5k_paths


python scripts/create_sample.py --output mboshi330 --files-list corpora/mboshi5k_paths --suffix mb.cleaned --size 330 --sets

scripts/create_folders.sh 5k 330 mboshi-french-parallel-corpus
rm corpora/mboshi5k_paths


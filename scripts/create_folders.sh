#!/bin/bash

in=$1
output=$2
path_folder=$3

declare -a ext=("en" "fr" "wav" "true_phones")
declare -a out=("train" "dev")

mkdir corpora/$path_folder/$output

for j in "${out[@]}"
do
   mkdir "corpora/$path_folder/$output/$j"
done

for i in "${ext[@]}"
do
  for j in "${out[@]}"
  do
       mkdir "corpora/$path_folder/$output/$j/$i"
  done
done

declare -a folder=("train" "dev" "test")

for i in "${ext[@]}"
do
   for j in "${folder[@]}"
   do
       for n in "${out[@]}"
       do
           if [ "$i" = "true_phones" ]
              then
                scripts/copy_files.sh "$n.ids" ".phn.unseg" "corpora/$path_folder/$in/$j/$i/" "corpora/$path_folder/$output/$n/$i/"
              else
                scripts/copy_files.sh "$n.ids" ".$i" "corpora/$path_folder/$in/$j/$i/" "corpora/$path_folder/$output/$n/$i/"
              fi
       done
   done
done

mv *ids corpora/$path_folder/$output/



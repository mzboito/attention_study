import codecs, glob, argparse
import math, random

'''
How it works:
1. reads corpus
2. transforms in (id, number of tokens)
4. generates buckets (number of tokens, number of members)
5. computes how many members to put in the new set for each bucket
    5.1. transforms the buckets into list (bucket, proportion)
    5.2. computes the number of sentences that proportion represents for the new size
    5.3. performs random to get the sentences in the bucket, avoiding identical sentences
6. write new list of ids
'''

sentence_id = 0
text = 1

def write_set(output_name, index, ids_list):
    with codecs.open(output_name,"w", "utf-8") as output_file:
        for element in ids_list:
            output_file.write(element[index] + "\n")

def write_dictionary(output_name, dictionary):
    with open(output_name,"w") as output_file:
        for key in dictionary.keys():
            output_file.write("{}\t{}\n".format(key, len(dictionary[key])))

def prune_set(sample, size):
    sample_size = sum(len(sample[key]) for key in sample.keys())
    if sample == size: #nothing to do
        return sample 

    #sort from the lowest prob to the highest
    buckets = sorted(generate_proportion(sample, sample_size), key=lambda x: x[1]) 
    index = 0
    #removes sentences until the desired size is reached
    #they are removed equally from all buckets in order to preserve the propostion
    while sample > size:
        if index == len(buckets):
            index = 0
        key, _ = buckets[index]
        sample[key] = sample[key][:-1] #removes the tail
        index += 1
        sample -= 1
    
    return sample

def check_candidate(info, index, sample): #index 
    for element in sample:
        if element[index] == info:
            return False
    return True

def sample_sentences(sentences, number2sample):
    #sentences are tuples of (sentence id, text)

    if number2sample == len(sentences):
        return sentences

    sample = []
    random.shuffle(sentences) 
    index = 0

    while number2sample > 0 and index < len(sentences):
        candidate = sentences[index]
        #avoids identical sentences
        if check_candidate(candidate, text, sample): 
            sample.append(candidate)
            number2sample-=1
        index +=1    

    index = 0
    while number2sample > 0:
        candidate = sentences[index]
        #if there are not enough different sentences, add identical sentences, but avoid adding twice the same sentence_id 
        if check_candidate(candidate, sentence_id, sample):
            sample.append(candidate)
            number2sample-=1
        index +=1

    return sample

def generate_proportion(corpus_dict, size_corpus):
    return [(key, (len(corpus_dict[key])/(size_corpus * 1.0)) ) for key in corpus_dict.keys()]

def down_sample(corpus_dict, new_size):
    SET = dict()
    size_corpus = sum(len(corpus_dict[key]) for key in corpus_dict.keys()) 
    buckets = generate_proportion(corpus_dict, size_corpus)

    for key, proportion in buckets: #avg_length, proportion
        number_sent = math.ceil(proportion * new_size)
        assert number_sent <= len(corpus_dict[key])
        SET[key] = sample_sentences(corpus_dict[key], number_sent)

    return prune_set(SET, new_size)

def read_file(f_path):
    #considers that each file has a single line of text
    #returns (sentence_key, text), number of tokens
    sentence = [line.strip("\n") for line in codecs.open(f_path,"r","utf-8")][0]
    return (f_path.split("/")[-1].split(".")[0], sentence), len(sentence.split(" "))

def read_corpus(path_list, suffix):
    files = []
    for folder in path_list:
        files += glob.glob(folder + "*." + suffix) if suffix else glob.glob(folder + "*")

    CORPUS = dict()
    for f_path in files:
        #reads the file
        id_sentence, number_tokens = read_file(f_path)
        #sort by number of tokens
        try:
            CORPUS[number_tokens] += [id_sentence]
        except KeyError:
            CORPUS[number_tokens] = [id_sentence]
    return CORPUS, len(files)

def run(args):
    args.size = int(args.size)
    path_list = [path.strip() for path in open(args.file_list, "r")]
    CORPUS, size = read_corpus(path_list, args.suffix)
    assert size > args.size, "Cannot perform down-sampling with this reduction value"
    sample = down_sample(CORPUS, args.size)
    write_set(args.output, sentence_id, sample)
    if args.stats:
        write_dictionary(args.output + ".sample.stats", sample)
        write_dictionary(args.output + ".corpus.stats", CORPUS)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--size', type=str, nargs='?', help='new size for the corpus')
    parser.add_argument('--file-list', type=str, nargs='?', help='a file containing the list of paths for all the folders where there are files to read')
    parser.add_argument('--suffix', type=str, nargs='?', help='optional, get only the files ending by a given suffix')
    parser.add_argument('--output', type=str, nargs='?', help='output name')
    parser.add_argument('--stats', type=str, nargs='?', help='includes some stats files')
    args = parser.parse_args()
    if not (args.size and args.suffix):
        parser.print_help()
        exit(1)
    run(args)
